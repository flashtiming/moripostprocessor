%
O9821(REN ANG XY SINGLE SURFACE)
G65P9724
#113=0
#149=0
#3=#5043-#116
#5=#5041
#6=#5042
IF[#1NE#0]GOTO1
#3000=91(A INPUT MISSING)
N1
IF[#7NE#0]GOTO2
#3000=91(D INPUT MISSING)
N2
IF[#19EQ#0]GOTO3
IF[#11EQ#0]GOTO3
#3000=91(SH INPUT MIXED)
N3
IF[#20EQ#0]GOTO4
IF[#13EQ#0]GOTO4
#3000=91(TM INPUT MIXED)
N4
IF[#19EQ#0]GOTO45
IF[#20EQ#0]GOTO45
#3000=91(ST INPUT MIXED)
N45
#14=135
WHILE[#14LT149]DO1
#[#14]=#0
#14=#14+1
END1
M98P9728
IF[#114EQ#115]GOTO22
G31X[#5-#502]Y[#6-#503]F#119
IF[ABS[#5061-[#5-#502]]GE#123]GOTO22
IF[ABS[#5062-[#6-#503]]GE#123]GOTO22
#113=0
N8
#28=#1
G65P9731A#28(#509 LOAD)
#30=-1
G65P9726A#28Q#17D[#7*2]S#509
IF[#149NE0]GOTO21
#135=#124+[#509*COS[#28]]+#502(COR X SKIP POS)
#135=[ROUND[#135*10000]]/10000
#136=#125+[#509*SIN[#28]]+#503(COR Y SKIP POS)
#136=[ROUND[#136*10000]]/10000
#138=SQRT[[#135-#5]*[#135-#5]+[#136-#6]*[#136-#6]](SIZE)
IF[#8EQ#0]GOTO9
#138=#138+#[2000+#8](SIZE COR)
N9
#140=#135-#5-[#7*COS[#28]](X ERROR)
#141=#136-#6-[#7*SIN[#28]](Y ERROR)
#143=#138-#7(SIZE ERROR)
#145=SQRT[[#140*#140]+[#141*#141]](TP ERROR)
#146=#143*#30(METAL CON)
#112=#30
IF[#23EQ#0]GOTO10(PRINT)
G65P9730D#7H#11M#13S#19T#20W#23X#5Y#6
N10
IF[#21EQ#0]GOTO11(U UPPER TOL)
IF[ABS[#143]GE#21]GOTO23
IF[#145GE#21]GOTO23
N11
IF[#11EQ#0]GOTO13
IF[ABS[#143]LT#11]GOTO12
#113=1
N12
IF[#120AND4EQ4]GOTO13
IF[ABS[#143]LT#11]GOTO13
G1X#5Y#6F#119
#[3006-[[#120AND8]/8*6]]=1(OUT OF TOL)
N13
IF[#13EQ#0]GOTO15
IF[ABS[#145]LT[#13/2]]GOTO14
#113=2
N14
IF[#120AND4EQ4]GOTO15
IF[ABS[#145]LT[#13/2]]GOTO15
G1X#5Y#6F#119
#[3006-[[#120AND8]/8*6]]=1(OUT OF POSITION)
N15
IF[#20EQ#0]GOTO19
IF[ABS[#146]LT#22]GOTO19(NULL BAND)
IF[#9NE#0]GOTO16
#9=1
N16
G65P9732T#20C[#146*#9]
N19
IF[#19EQ#0]GOTO24
G65P9732S#19W1.0
GOTO24
N21
G1X#5Y#6F#119
IF[#149NE2]GOTO22
#3000=93(PROBE FAIL)
N22
#3000=92(PROBE OPEN)
N23
G1X#5Y#6F#119
#113=3
IF[#120AND4EQ4]GOTO24
#[3006-[[#120AND8]/8*6]]=1(UPPER TOL EXCEEDED)
N24
G1X#5Y#6F#119
M99

%
%
O9857(REN*TOOL*AUTO*SET)
M5
M98P9750
IF[#18EQ#0]GOTO1818
#125=#18
N1818
IF[#7LT[35.*#129]]GOTO111
IF[#26LT10.2*#129]GOTO111
M0
(POTENTIAL*COLLISION)
N111
#146=0
IF[#2NE#0]GOTO1
#2=1
N1IF[#2EQ1]GOTO22
#149=120
IF[#7EQ#0]GOTO199
N22
IF[#7NE#0]GOTO2
#7=0
N2IF[#7GT0.]GOTO4
#19=3
GOTO44
N4#19=4
N44
IF[#20NE#0]GOTO10
#20=#4120
N10(IF*T*D*INPUT)
IF[#2EQ1]GOTO12
N11(B*TYPE)
IF[#8NE#0]GOTO12
IF[#102EQ2.]GOTO111
#149=90.
M98P9759
N111
#8=#20(D)
N12
#7=#7/2
N13
IF[#25EQ#0]GOTO556
#[#140+#20]=0(LW)
#[#141+#20]=#25(LG)
N556
#7=ABS[#7]
IF[#7EQ0]GOTO9
N5
#130=800(RPM*SECOND*TOUCH)
#132=[#7*2]/#129
#132=[ROUND[1910/#132]]*10
IF[#132LT800]GOTO6
#132=800(RPM*FIRST*TOUCH)
N6
IF[#132GT150]GOTO7
#132=150
N7
#9=#132/8*#129(FEED*FIRST*TOUCH)
#144=4.*#129(FEED*SECOND*TOUCH)
N9
M98P9751
N16(LENGTH*SET)
IF[ABS[#7*2]LE#110]GOTO17
IF[ABS[#104]EQ1.]GOTO117
G65P9754X[#137-[#7*#103]]Y[#136]
GOTO118
N117G65P9754X[#137]Y[#136-[#7*#103]]
GOTO118
N17G65P9754X#137Y#136
N118
IF[#118EQ0]GOTO13
(OTS*PROBE*0N)
G4X1.0
N13
IF[#2EQ#0]GOTO19
IF[#2EQ1]GOTO19
IF[#2EQ2]GOTO216
IF[#2EQ3]GOTO19
IF[#2EQ4]GOTO416
N19
IF[#7NE0]GOTO119
IF[#138EQ0]GOTO119
#135=[[#[#120]/#107*#129]-#14]
#135=#135-[#112*2]
G65P9754Z[#135+[#138*#100]-#116]
G65P9753A#123T[#135+[#139*#100]-#116]F#124
#130=#131
G65P9754Z[#130+[[5.*#129]*#100]]
G65P9752A#123T#130F[30*#129]Q#17
#10=[[#[#120]/#107*#129]-#131-#14]
#10=#10-#112
#30=#10
GOTO210
N119
IF[#138EQ0]GOTO120
N190#149=140.
N199M98P9759
N120
#135=[[#[#120]/#107*#129]-#14+[#113*#100]]-#112
G65P9754Z#135
G65P9753A#123T[[#[#120]/#107*#129]-#14+[#114*#100]-#112]F#127W1.
IF[ABS[#7*2]GT#110]GOTO160
(STATIC)
N150
G65P9752A#123T[[#[#120]/#107*#129]-#14-#112]F[30*#129]D#101Q#17
#10=[#[#120]/#107*#129]-#131-#14
#10=#10-#112
GOTO170
N160
S#132
M#19
(ROTATING)
G65P9752A#123T[[[#[#120]]/#107*#129]-#14-#112]F#144D#9Q#17S#130
#10=[[#[#120]/#107*#129]-#131-#14]
#10=#10+[#[#120+6]/#107*#129]
#10=#10-#112
N170
#30=#10
IF[#2EQ3]GOTO216
GOTO210
N216(DIAMETER*SET)
IF[#108EQ44]GOTO2217
#10=#10*[-1]
N2217
IF[#23EQ#0]GOTO2216
#23=#23+#114
GOTO2217
N2216#23=#114
N2217
G65P9753A#123T[[#[#120]/#107*#129]-#14+[[#23+#10]*#100]-#112]F#127W1.
G65P9753A#121T[#137]F#127W1.
G65P9753A#122T[#136]F#127W1.
S#132
M#19
IF[#26NE#0]GOTO217
#26=5.*#129
N217
IF[ABS[#7*2]GT#111]GOTO250
N220
(DOUBLE*SIDED)
IF[ABS[#104]EQ1.]GOTO221
(X*AXIS)
G65P9753A#121T[#137-[[#133+#125+ABS[#7]]*#115]]F#127W1.
G65P9753A#123T[[#[#120]/#107*#129]-#14-[[#26-#10]*#100]-#112]F#127W1.
G65P9752A#121T[#137-[[#133+ABS[#7]]*#115]]F#144D#9Q#17S#130
#21=#137-#131-[[#133+[#[#120+6]]/#107*#129]*#115]
G65P9753A#123T[[#[#120]/#107*#129]-#14+[[#23+#10]*#100]-#112]F#127W1.
S#132
M#19
G65P9753A#121T[#137+[[#133+#125+ABS[#7]]*#115]]F#127W1.
G65P9753A#123T[[#[#120]/#107*#129]-#14-[[#26-#10]*#100]-#112]F#127W1.
G65P9752A#121T[#137+[[#133+ABS[#7]]*#115]]F#144D#9Q#17S#130
#12=#131
G65P9753A#123T[[#[#120]/#107*#129]-#14+[[#23+#10]*#100]-#112]F#127W1.
#12=#137-#12+[[#133+[#[#120+6]]/#107*#129]*#115]
#21=ABS[#21]
#12=ABS[#12]
#12=[#12+#21]/2
GOTO300
N221(Y*AXIS)
G65P9753A#122T[#136-[[#134+#125+ABS[#7]]*#115]]F#127W1.
G65P9753A#123T[[#[#120]/#107*#129]-#14-[[#26-#10]*#100]-#112]F#127W1.
G65P9752A#122T[#136-[[#134+ABS[#7]]*#115]]F#144D#9Q#17S#130
#21=#136-#131-[[#134+[#[#120+6]]/#107*#129]*#115]
G65P9753A#123T[[#[#120]/#107*#129]-#14+[[#23+#10]*#100]-#112]F#127W1.
S#132
M#19
G65P9753A#122T[#136+[[#134+#125+ABS[#7]]*#115]]F#127W1.
G65P9753A#123T[[#[#120]/#107*#129]-#14-[[#26-#10]*#100]-#112]F#127W1.
G65P9752A#122T[#136+[[#134+ABS[#7]]*#115]]F#144D#9Q#17S#130
#12=#131
G65P9753A#123T[[#[#120]/#107*#129]-#14+[[#23+#10]*#100]-#112]F#127W1.
#12=#136-#12+[[#134+[[#[#120+6]]/#107*#129]]*#115]
#21=ABS[#21]
#12=ABS[#12]
#12=[#12+#21]/2
GOTO300
N250(SINGLE*SIDED)
IF[ABS[#104]EQ2.]GOTO231
(Y*AXIS)
G65P9753A#122T[#136-[[#134+#125+ABS[#7]]*#103]]F#127W1.
G65P9753A#123T[[#[#120]/#107*#129]-#14-[[#26-#10]*#100]-#112]F#127W1.
G65P9752A#122T[#136-[[#134+ABS[#7]]*#103]]F#144D#9Q#17S#130
#12=#136-#131
#12=#12-[[#134+[[#[#120+6]]/#107*#129]]*#103]
#12=#12*#103
GOTO300
N231(X*AXIS)
G65P9753A#121T[#137-[[#133+#125+ABS[#7]]*#103]]F#127W1.
G65P9753A#123T[[#[#120]/#107*#129]-#14-[[#26-#10]*#100]-#112]F#127W1.
G65P9752A#121T[#137-[[#133+ABS[#7]]*#103]]F#144D#9Q#17S#130
#12=#137-#131
#12=#12-[[#133+[[#[#120+6]]/#107*#129]]*#103]
#12=#12*#103
N300
GOTO210
N416(MEASURE*UP)
S#132
M#19
IF[#21NE#0]GOTO418
#21=2.*#129
N418
IF[ABS[#104]EQ2.]GOTO117
G65P9754X[#137-[[#7+#133+#125]*#115]]Y[#136]
GOTO118
N117G65P9754X[#137]Y[#136-[[#7+#134+#125]*#115]]
N118
#135=[[#[#120]]/#107*#129]-#14+[#113*#100]-#112
G65P9754Z#135
S#132
M#19
G65P9753A#123T[[#[#120+5]/#107*#129]-#14-[#114*#100]-#112]F#127W1.
(ROTATING)
IF[ABS[#104]EQ2.]GOTO117
G65P9753A#121T[#137-[[#133+#7-#21]*#115]]F#127W1.
GOTO118
N117G65P9753A#122T[#136-[[#134+#7-#21]*#115]]F#127W1.
N118
G65P9752A#123T[[#[#120+5]/#107*#129]-#14-#112]F#144D#9Q#17S#130
#10=[[#[#120+5]]/#107*#129]-#131-#14
#10=#10+[[#[#120+6]]/#107*#129]
#10=#10-#112
IF[ABS[#104]EQ2.]GOTO127
G65P9753A#121T[#137-[[#133+#7+#125]*#115]]F#127W1.
GOTO128
N127G65P9753A#122T[#136-[[#134+#7+#125]*#115]]F#127W1.
N128
G65P9753A#123T[[#[#120]/#107*#129]-#14+[#114*#100]-#112]F#127W1.
#30=#10
N210
M98P9751
N400G90
(TOOL*OFFSET*UPDATE)
IF[#2EQ2]GOTO30
(LEN*SET)
IF[#11EQ#0]GOTO29
IF[ABS[#30]LE#11]GOTO29
#149=130
#146=1
IF[#13NE#0]GOTO30
GOTO199
N29
#116=#116*#100
#30=#30*#100
#[#141+#20]=[#116+#112-#30-#6](A*TYPE*LG)
IF[#140EQ0]GOTO30
#[#140+#20]=0(LW)
N30IF[#2EQ1]GOTO500
IF[#2EQ4]GOTO500
(RAD*SET)
N33IF[#11EQ#0]GOTO36
IF[#142EQ0]GOTO34
IF[ABS[#[#143+#8]+#[#142+#8]-#12]LE#11]GOTO36
GOTO35
N34IF[ABS[#[#143+#8]-#12]LE#11]GOTO36
N35#149=130
#146=1
IF[#13NE#0]GOTO37
GOTO199
N36
#[#143+#8]=[#12-#5]*#109(A*TYPE*RG)
IF[#142EQ0]GOTO37
#[#142+#8]=0(RW)
N37
N500
M98P9751
N40G90
IF[#146EQ0]GOTO42
IF[#13NE#0]GOTO42
N199M98P9759
N42#149=#0
M5
(OTS*PROBE*OFF)
N55
M99



%